#include "renderer.h"

#include <imgui_vita.h>
#include <vitaGL.h>
#include <math.h>
#include <stdio.h>
#include <string>

#define BUF_OFFS(i) ((void*)(i))

float vbo[9*2];
GLuint buffers[2];

Renderer::Renderer(){
    // Init VitaGL
    vglInit(0x800000);
    vglWaitVblankStart(GL_FALSE);

    // Init ImGui
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui_ImplVitaGL_Init();
    ImGui::StyleColorsDark();
    ImGui_ImplVitaGL_GamepadUsage(false);

    // Generate and fill buffers
    glGenBuffers(2, buffers);
    
    // VBO
    memcpy(&vbo[9*0], &vertices[0], sizeof(float) * 9);
    memcpy(&vbo[9*1], &colors[0], sizeof(float) * 9);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 9 * 2, vbo, GL_STATIC_DRAW);

    // Indicies
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * 3, indices, GL_STATIC_DRAW);

    glClearColor(0, 0, 0, 0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    init_perspective(90.0f, 960.0f/544.0f, 0.001, 1000);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glViewport(0, 0, 960, 544);

    renderThread_ = sceKernelCreateThread("Render Thread", this->render, 0x10000100, 0x10000, 0, 0, NULL);
    sceKernelStartThread(renderThread_, 0, 0);
}

Renderer::~Renderer(){
    ImGui_ImplVitaGL_Shutdown();
    ImGui::DestroyContext();
    vglEnd();
}

void Renderer::init_perspective(float fov, float aspect, float near, float far){
    float half_height = near * tanf(((fov * M_PI) / 180.0f) * 0.5f);
    float half_width = half_height * aspect;

    glFrustum(-half_width, half_width, -half_height, half_height, near, far);
}

static void Renderer::render(){
    for(;;){
        vglStartRendering();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        {
            // Our OpenGL Stuff
            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_COLOR_ARRAY);

            glVertexPointer(3, GL_FLOAT, 0, BUF_OFFS(0));
            glColorPointer(3, GL_FLOAT, 0, BUF_OFFS(9*sizeof(float)));
            glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, BUF_OFFS(0));

            glDisableClientState(GL_VERTEX_ARRAY);
            glDisableClientState(GL_COLOR_ARRAY);
        }
        renderer.render_imgui();
        vglStopRendering();
        glLoadIdentity();
    }
}

void Renderer::render_imgui(){
    ImGui_ImplVitaGL_NewFrame();

    // FPS Counter / Stats window
    bool open = true;

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    //window_flags |= ImGuiWindowFlags_NoScrollBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;

    ImGui::SetNextWindowSize(ImVec2(250,75));
    ImGui::SetNextWindowPos(ImVec2(625, 25));

    ImGui::Begin("Stats", &open, window_flags);
    ImGui::Text("%.3f ms (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();

    ImGui::Render();
    ImGui_ImplVitaGL_RenderDrawData(ImGui::GetDrawData());
}