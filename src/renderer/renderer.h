#include <psp2/kernel/threadmgr.h>
#include <vector>

#ifndef RENDERER_H
#define RENDERER_H

class Renderer{
  public:

    std::vector<float> colors = {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    };

    std::vector<float> vertices = {
        0.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0
    };

    uint16_t indices[3] = {
        0, 1, 2
    };

    Renderer();
    ~Renderer();

  private:
    SceUID renderThread_;

    void init_perspective(float fov, float aspect, float near, float far);
    static void render();
    void render_imgui();
};

inline Renderer renderer;
#endif